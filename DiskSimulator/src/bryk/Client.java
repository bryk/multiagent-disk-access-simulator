package bryk;

import sim.engine.SimState;
import sim.engine.Steppable;

public class Client implements Steppable {
  private static final long serialVersionUID = 1L;
  private long id;
  private boolean finished = true;

  public Client(long id, DiskSimulator simulator) {
    this.id = id;
  }

  public void setFinished(boolean finished) {
    this.finished = finished;
  }

  @Override
  public void step(SimState state) {
    if (finished) {
      DiskSimulator simulator = (DiskSimulator) state;
      Disk disk = simulator.doGetRandomDisk();
      finished = false;
      if (simulator.random.nextBoolean()) {
        disk.readFile(this);
      } else {
        disk.writeFile(this, simulator.nextFileSize());
      }
    }
  }

  public long getId() {
    return id;
  }
}
