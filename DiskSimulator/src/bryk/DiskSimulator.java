package bryk;

import java.util.ArrayList;
import java.util.List;

import sim.engine.SimState;

public class DiskSimulator extends SimState {
  private static final long serialVersionUID = 1L;

  private int diskCount = 200;
  private int clientCount = 8000;
  private long speedIncreaseParam = 10;

  private List<Disk> disks;
  private List<Client> clients;

  @Override
  public void start() {
    super.start();
    disks = new ArrayList<Disk>();
    clients = new ArrayList<Client>();
    long id = 1;
    for (int i = 0; i < diskCount; i++) {
      Disk disk = new Disk(id++, this);
      disks.add(disk);
      schedule.scheduleRepeating(disk);
    }

    for (int i = 0; i < clientCount; i++) {
      Client client = new Client(id++, this);
      clients.add(client);
      schedule.scheduleRepeating(client);
    }
  }

  @Override
  public void finish() {
    super.finish();
  }

  public DiskSimulator(long seed) {
    super(seed);
  }

  public static void main(String[] args) {
    doLoop(DiskSimulator.class, args);
    System.exit(0);
  }

  public Disk doGetRandomDisk() {
    return disks.get(random.nextInt(disks.size()));
  }

  public List<Disk> doGetDisks() {
    return disks;
  }

  public int getDiskCount() {
    return diskCount;
  }

  public void setDiskCount(int diskCount) {
    this.diskCount = diskCount;
  }

  public int getClientCount() {
    return clientCount;
  }

  public void setClientCount(int clientCount) {
    this.clientCount = clientCount;
  }

  public long nextFileSize() {
    return random.nextLong(100) * 500;
  }

  public long getSpeedIncreaseParam() {
    return speedIncreaseParam;
  }

  public void setSpeedIncreaseParam(long speedIncreaseParam) {
    this.speedIncreaseParam = speedIncreaseParam;
  }
}
