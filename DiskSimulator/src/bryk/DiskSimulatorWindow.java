package bryk;

import java.net.MalformedURLException;

import javax.swing.JFrame;
import javax.swing.UIManager;

import sim.display.Controller;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.util.media.chart.ScatterPlotGenerator;

public class DiskSimulatorWindow extends GUIState {
  private ScatterPlotGenerator chartRequested;
  private JFrame chartRequestedFrame;
  private ScatterPlotGenerator chartRates;
  private JFrame chartRatesFrame;
  private double maxRead = 0;
  private double maxWrite = 0;
  private double maxToRead = 0;
  private double maxToWrite = 0;
  private double read[][];
  private double toRead[][];
  private double write[][];
  private double toWrite[][];

  public static void main(String[] args) {
    try {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
    new DiskSimulatorWindow().createController();
  }

  public DiskSimulatorWindow() {
    super(new DiskSimulator(System.currentTimeMillis()));
  }

  public static String getName() {
    return "Disk Simulator";
  }

  public static Object getInfo() {
    try {
      System.out.println(new java.io.File("./etc/info.html").toURI().toURL());
      return new java.io.File("etc/info.html").toURI().toURL();
    } catch (MalformedURLException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public Object getSimulationInspectedObject() {
    return state;
  }

  @Override
  public void start() {
    super.start();
    setupPortrayals();
  }

  @Override
  public void load(SimState state) {
    super.load(state);
    setupPortrayals();
  }

  public void setupPortrayals() {
    chartRequested.removeAllSeries();
    chartRates.removeAllSeries();
    maxRead = 0;
    maxWrite = 0;
    maxToRead = 0;
    maxToWrite = 0;
    DiskSimulator simulator = (DiskSimulator) state;
    read = new double[2][simulator.getDiskCount()];
    toRead = new double[2][simulator.getDiskCount()];
    write = new double[2][simulator.getDiskCount()];
    toWrite = new double[2][simulator.getDiskCount()];

    for (int i = 0; i < simulator.getDiskCount(); i++) {
      toRead[0][i] = i;
      read[0][i] = i;
      write[0][i] = i;
      toWrite[0][i] = i;
    }
    chartRates.addSeries(read, "Read rate (bytes/sec)", null);
    chartRequested.addSeries(toRead, "Bytes to read", null);
    chartRates.addSeries(write, "Write rate (bytes/sec)", null);
    chartRequested.addSeries(toWrite, "Bytes to write", null);
    scheduleRepeatingImmediatelyBefore(new Steppable() {
      private static final long serialVersionUID = 1L;

      @Override
      public void step(SimState state) {
        DiskSimulator simulator = (DiskSimulator) state;
        for (int i = 0; i < simulator.getDiskCount(); i++) {
          Disk disk = simulator.doGetDisks().get(i);
          read[1][i] = disk.getLastStepBytesRead();
          if (read[1][i] > maxRead)
            maxRead = read[1][i];
          toRead[1][i] = disk.getLastStepBytesToRead();
          if (toRead[1][i] > maxToRead)
            maxToRead = toRead[1][i];
          write[1][i] = disk.getLastStepBytesWritten();
          if (write[1][i] > maxWrite)
            maxWrite = write[1][i];
          toWrite[1][i] = disk.getLastStepBytesToWrite();
          if (toWrite[1][i] > maxToWrite)
            maxToWrite = toWrite[1][i];
        }
        chartRequested.updateChartWithin(state.schedule.getSteps(), 30);
        chartRates.setYAxisRange(0, Math.max(15000, Math.max(maxRead, maxWrite)));
        chartRates.updateChartWithin(state.schedule.getSteps(), 30);
      }
    });
  }

  @Override
  public void init(final Controller c) {
    super.init(c);
    System.out.println("INIT");
    chartRates = new sim.util.media.chart.ScatterPlotGenerator();
    chartRates.setTitle("Rates in bytes");
    chartRates.setYAxisRange(0, 15000);
    chartRates.setXAxisLabel("Disk no");
    chartRates.setYAxisLabel("Transfer speed in bytes/sec");
    chartRequested = new sim.util.media.chart.ScatterPlotGenerator();
    chartRequested.setTitle("Requested bytes");
    chartRequested.setYAxisLabel("Bytes");
    chartRequested.setXAxisLabel("Disk no");
    chartRequested.setYAxisRange(0, 1000000);
    chartRatesFrame = chartRates.createFrame();
    chartRatesFrame.setVisible(true);
    chartRequestedFrame = chartRequested.createFrame();
    chartRequestedFrame.setVisible(true);
    c.registerFrame(chartRequestedFrame);
    c.registerFrame(chartRatesFrame);
  }
}
