package bryk;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;

import sim.engine.SimState;
import sim.engine.Steppable;

public class Disk implements Steppable {
  private static final long serialVersionUID = 1L;

  private long readSpeed = 0; // bytes/second
  private long writeSpeed = 0; // bytes/second
  private long diskSize; // bytes
  private long id;
  private List<Pair<Client, Long>> readers = new ArrayList<Pair<Client, Long>>();
  private List<Pair<Client, Long>> writers = new ArrayList<Pair<Client, Long>>();
  private DiskSimulator simulator;
  private long lastStepBytesRead = 0;
  private long lastStepBytesToRead = 0;
  private long lastStepBytesWritten = 0;
  private long lastStepBytesToWrite = 0;

  public Disk(long id, DiskSimulator simulator) {
    this.id = id;
    this.simulator = simulator;
  }

  public void readFile(Client client) {
    readers.add(Pair.of(client, simulator.nextFileSize()));
  }

  public void writeFile(Client client, long size) {
    writers.add(Pair.of(client, size));
  }

  @Override
  public void step(SimState state) {
    lastStepBytesRead = 0;
    lastStepBytesToRead = 0;
    if (readers.size() > 0) {
      List<Pair<Client, Long>> newReaders = new ArrayList<Pair<Client, Long>>();
      long currentReadSpeed = (readSpeed + readers.size() - 1) / readers.size();
      for (Pair<Client, Long> read : readers) {
        if (lastStepBytesRead + currentReadSpeed > readSpeed) {
          currentReadSpeed = readSpeed - lastStepBytesRead;
        }
        long toRead = read.getValue() - currentReadSpeed;
        if (toRead <= 0) {
          read.getKey().setFinished(true);
          lastStepBytesRead += read.getValue();
        } else {
          lastStepBytesRead += currentReadSpeed;
          newReaders.add(Pair.of(read.getKey(), toRead));
          lastStepBytesToRead += toRead;
        }
      }
      readers = newReaders;
    }

    lastStepBytesWritten = 0;
    lastStepBytesToWrite = 0;
    if (writers.size() > 0) {
      List<Pair<Client, Long>> newWriters = new ArrayList<Pair<Client, Long>>();
      long currentWriteSpeed = (writeSpeed + writers.size() - 1) / writers.size();
      for (Pair<Client, Long> write : writers) {
        if (lastStepBytesWritten + currentWriteSpeed > writeSpeed) {
          currentWriteSpeed = writeSpeed - lastStepBytesWritten;
        }
        long toWrite = write.getValue() - currentWriteSpeed;
        if (toWrite <= 0) {
          write.getKey().setFinished(true);
          lastStepBytesWritten += write.getValue();
        } else {
          lastStepBytesWritten += currentWriteSpeed;
          newWriters.add(Pair.of(write.getKey(), toWrite));
          lastStepBytesToWrite += toWrite;
        }
      }
      writers = newWriters;
    }
    long speed = (long) ((simulator.schedule.getSteps() * simulator.getSpeedIncreaseParam()) * ((double) id / (double) simulator
        .getDiskCount()));

    readSpeed = speed;
    writeSpeed = speed;
  }

  public long getLastStepBytesToRead() {
    return lastStepBytesToRead;
  }

  public long getLastStepBytesRead() {
    return lastStepBytesRead;
  }

  public long getLastStepBytesToWrite() {
    return lastStepBytesToWrite;
  }

  public long getLastStepBytesWritten() {
    return lastStepBytesWritten;
  }

  public long getReadSpeed() {
    return readSpeed;
  }

  public long getWriteSpeed() {
    return writeSpeed;
  }

  public long getId() {
    return id;
  }

  public long getDiskSize() {
    return diskSize;
  }
}
